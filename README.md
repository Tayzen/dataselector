![](img/rounded_logo.png)

-----------------

# Funnel

[![Pipeline status](https://gitlab.com/Tayzen/funnel/badges/master/pipeline.svg)](https://gitlab.com/Tayzen/funnel/-/commits/master)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)

## What is it?

`funnel` provide an easy to use, easy to read framework to create very complex data selections over pandas DataFrames (and probably more to come like koalas, PySpark or dask).

As the backend of the library is pandas, the API can easily be used with DataFrames or Series.

This library does **NOT** allow you to make transformation on your data.

## How I get it? (not yet available)

You can add it to your project easily with pip (or the dependency manager of your choice), for instance you can use these cammands:

```sh
pip install --user funnel
pipenv install funnel
poetry add funnel
```

## Usage

```python
from funnel import Column

# 1. Initialization
df = pd.DataFrame({
    "id": [1, 2, 3, 4, 5, 6],
    "col1": [1, 0, 8, 5, 3, 2],
    "col2": ["a", "b", "c", "d", "e", "f"], 
})
df2 = pd.DataFrame({
    "id": [1, 2, 3, 4, 7, 8],
    "col1": [1, 0, 8, 5, 3, 2],
    "col3": ["a", "b", "c", "d", "e", "f"], 
})

# 2. Defining Columns handler objects
col1 = Column(df, "id", "col1")
col2 = Column(df, "id", "col2")
col3 = Column(df2, "id", "col3")

# 3. Creating the filter
big_fucking_filter = (col1 <= 3) & col2.is_in(["a", "b", "f"]) | ~ (col3.is_in(["a", "b", "e"]))

# 4. Generating the filtered DataFrame
print(big_fucking_filter.get_df())
# Result:
#    id  col1 col2 col3
# 0   1   1.0    a    a
# 1   2   0.0    b    b
# 2   3   8.0    c    c
# 3   4   5.0    d    d
# 4   6   2.0    f  NaN
# 5   8   NaN  NaN    f
```

This very simple code realizes the following operations:

1. 2 pandas DataFrame are initialized
2. Defining the column into the framework's classes
3. Making the filter with an easy to read syntax
4. Executing the filtering

## Deeper explanation

First, you need to define a `Column` object, which takes as parameters:

* A pandas DataFrame containing the data
* An ID column name (which will be used to select data)
* The name of the column of interest

On that `Column`, you can apply your comparison operations. When you do so, it produces a resulting `Filter` object. These filters can be combined to create complex filters. They also can be executed to return an usable result.

### `Column` operations

Many operations are available, everyone can't be used with all the types of data:

* `is_empty`: `Object`
* `identity`: `Object`
* `is_in`: `Object`
* `is_decimal`: numeric types (`int`, `float`, `double`, `numpy.int8`, etc...)
* `__gt__`: numeric types (`int`, `float`, `double`, `numpy.int8`, etc...), `date`, `datetime` and all the types with `__gt__` implemented
* `__ge__`: numeric types (`int`, `float`, `double`, `numpy.int8`, etc...), `date`, `datetime` and all the types with `__ge__` implemented
* `__lt__`: numeric types (`int`, `float`, `double`, `numpy.int8`, etc...), `date`, `datetime` and all the types with `__lt__` implemented
* `__le__`: numeric types (`int`, `float`, `double`, `numpy.int8`, etc...), `date`, `datetime` and all the types with `__le__` implemented
* `__eq__`: numeric types (`int`, `float`, `double`, `numpy.int8`, etc...), `str`, `date`, `datetime` and all the types with `__eq__` implemented
* `__ne__`: numeric types (`int`, `float`, `double`, `numpy.int8`, etc...), `str`, `date`, `datetime` and all the types with `__ne__` implemented

You must be careful with the operation you use, if you apply an incompatible operation for your dtype, an exception will be raised: `IncompatibleOperationException`

The usage of these operations generates a `Filter` object. So each `Column` acts as an [object factory](https://en.wikipedia.org/wiki/Factory_method_pattern).

### Combining simple filters

When your filters have been generated using an operation of the `Column` class, you have 2 choices:

1. Directly using the execution method to get the result of your filtering
2. Combining lots of filters to create a complex request

If you're using this framework, chances are that you want to use a combination of multiples filters. To do so, it's really simple, you can use 3 operations (each one with 2 API that you can use as you prefer):

* `&` (or `intersect`): It creates the intersections between the selected data of each filter
* `|` (or `union`): It creates the union between the selected data of each filter
* `~` (or `invert`): It applies only on one filter. It creates the complementary data selection: all data not selected by the filter will be selected.

These operations can obviously be combined. When you use the syntax with `~`, `&` and `|`, the operations are executed with the rules defined by Python: so `~` > `&` > `|`.

### Executing filtering

There are 2 modes of execution:

1. Generating a DataFrame with of the variables of interest used in your filtering operation
2. Get all the selected IDs as a Python Set to make your computation yourself
