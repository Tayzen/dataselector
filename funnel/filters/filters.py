from __future__ import annotations

from collections.abc import Callable
from dataclasses import dataclass

import pandas as pd

from funnel.exceptions import NotSameIdColumnsException
from funnel.filters.combination_op import CombinationOp


def reset_df_index(df):
    df.index = list(range(len(df)))
    return df


@dataclass
class CallableFilter:
    """
    A filter which can apply a filtering function on the items of the df.
    It can return a filtered df or all the selected IDs.
    """

    id_colname: str

    def intersect(self, other_filter: CallableFilter):
        return MetaFilter(self, other_filter, CombinationOp.INTERSECTION)

    def union(self, other_filter: CallableFilter):
        return MetaFilter(self, other_filter, CombinationOp.UNION)

    def invert(self):
        raise NotImplementedError

    def get_df(self):
        raise NotImplementedError

    def get_selected_ids(self):
        raise NotImplementedError

    def copy(self):
        return CallableFilter(self.id_colname)

    def __and__(self, other: CallableFilter):
        return self.intersect(other)

    def __or__(self, other: CallableFilter):
        return self.union(other)

    def __invert__(self):
        return self.invert()

    def __apply__(self):
        return self.get_df()


@dataclass
class Filter(CallableFilter):
    """
    A filter which can apply a filtering function on the items of the df.
    It can return a filtered df or all the selected IDs.
    """

    df: pd.DataFrame
    id_colname: str
    data_colname: str
    filtering_fun: Callable[[object], bool]

    def get_df(self):
        """
        Get the filtered DataFrame
        """
        return reset_df_index(
            self.df[self.df[self.data_colname].map(self.filtering_fun)]
        )

    def get_selected_ids(self):
        """
        Get the selected IDs
        """
        return set(self.get_df()[self.id_colname])

    def copy(self):
        return Filter(self.id_colname, self.df, self.data_colname, self.filtering_fun)

    def invert(self):
        res = self.copy()
        res.filtering_fun = lambda x: not self.filtering_fun(x)
        return res


class MetaFilter(CallableFilter):
    """
    A filter combination
    It can return a filtered df or all the selected IDs.
    """

    def __init__(
        self,
        filter1: CallableFilter,
        filter2: CallableFilter,
        combination_operation: CombinationOp,
    ):
        if (filter1.id_colname) != (filter2.id_colname):
            raise NotSameIdColumnsException
        self._filter1 = filter1
        self._filter2 = filter2
        self.id_colname = self._filter1.id_colname
        self.mode = combination_operation
        self._df = None

    @property
    def df(self):
        if self._df is None:
            # get the columns names in filter2 not in filter1 + the id column
            cols_to_use = self._filter2.df.columns.difference(
                self._filter1.df.drop(columns=[self.id_colname]).columns
            )
            # merging only with useful columns
            self._df = self._filter1.df.merge(
                self._filter2.df[cols_to_use],
                on=self.id_colname,
                how="outer",
                copy=False,  # could use less RAM
            )
        return self._df

    def get_selected_ids(self):
        """
        Get the selected IDs
        """
        return self.mode.execute(
            self._filter1.get_selected_ids(), self._filter2.get_selected_ids()
        )

    def get_df(self):
        """
        Get the filtered DataFrame
        """
        return reset_df_index(
            self.df[self.df[self.id_colname].isin(self.get_selected_ids())]
        )

    def copy(self):
        return MetaFilter(self._filter1, self._filter2, self.mode)

    def invert(self):
        return MetaFilter(~self._filter1, ~self._filter2, ~self.mode)
