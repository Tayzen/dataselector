class NotSameIdColumnsException(Exception):
    pass


class IncompatibleOperationException(Exception):
    pass
