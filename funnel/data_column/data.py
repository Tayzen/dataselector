import sys
from dataclasses import dataclass
from typing import Iterable, Optional

if sys.version_info < (3, 9, 0):
    from typing import Callable
else:
    from collections.abc import Callable


import pandas as pd

from funnel.exceptions import IncompatibleOperationException
from funnel.filters.filters import Filter

# TODO: test inferior versions


def check_compatibility(
    operation: Optional[str] = None, numeric_check=False, not_str_check=False
) -> Filter:
    def check_types(self):
        error_detected = numeric_check
        if numeric_check and pd.api.types.is_numeric_dtype(self.column):
            error_detected = False
        if not_str_check and pd.StringDtype.is_dtype(self.column.convert_dtypes()):
            error_detected = True

        if error_detected:
            raise IncompatibleOperationException

    def decorator(func):
        def exec(self, *args, **kwargs):
            if (operation or func.__name__) in dir(self.column):
                check_types(self)
                if operation is not None:
                    return func(self, *args, **kwargs)
                return self._method_builder(
                    lambda x: getattr(x, func.__name__)(*args, **kwargs)
                )

        return exec

    return decorator


@dataclass
class Column:
    """
    Columns composed of any types of objects
    """

    df: pd.DataFrame
    id_colname: str
    data_colname: str
    __dtype = None  # TODO: let possibility to override

    @property
    def dtype(self) -> type:
        if not self.__dtype:
            ser = self.df[self.data_colname]
            self.__dtype = ser.convert_dtypes().dtype
        return self.__dtype

    @property
    def column(self):
        return self.df[self.data_colname]

    def _method_builder(self, fun: Callable[[object], bool]) -> Filter:
        return Filter(
            df=self.df,
            id_colname=self.id_colname,
            data_colname=self.data_colname,
            filtering_fun=fun,
        )

    def is_empty(self) -> Filter:
        return self._method_builder(lambda x: not pd.notna(x))

    def identity(self) -> Filter:
        return self._method_builder(lambda x: True)

    def is_in(self, collection: Iterable) -> Filter:
        return self._method_builder(lambda x: x in collection)

    @check_compatibility("__mod__", numeric_check=True)
    def is_decimal(self) -> Filter:
        return self._method_builder(lambda x: x % 1 == 0)

    @check_compatibility(not_str_check=True)
    def __gt__(self, other) -> Filter:
        pass

    @check_compatibility(not_str_check=True)
    def __ge__(self, other) -> Filter:
        pass

    @check_compatibility(not_str_check=True)
    def __lt__(self, other) -> Filter:
        pass

    @check_compatibility(not_str_check=True)
    def __le__(self, other) -> Filter:
        pass

    @check_compatibility(not_str_check=True)
    def __eq__(self, other) -> Filter:
        pass

    @check_compatibility(not_str_check=True)
    def __ne__(self, other) -> Filter:
        pass

    def __bool__(self) -> Filter:
        return True

    def __repr__(self) -> str:
        return f"Column(id={self.id_colname}, name={self.data_colname}, dtype={self.dtype}, dtypes={self.df.dtypes})"

    def __str__(self) -> str:
        return f"Column(id={self.id_colname}, name={self.data_colname}, dtype={self.dtype})"
