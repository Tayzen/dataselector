from datetime import datetime
from test.conftest import RandomClass

import numpy as np
import pandas as pd
import pytest

from funnel.data_column.data import Column


@pytest.fixture
def filter_age_inf_40(column_age):
    return column_age < 40


@pytest.fixture
def filter_age_sup_16(column_age):
    return column_age > 16


@pytest.fixture
def simple_metafilter(filter_age_inf_40, filter_age_sup_16):
    return filter_age_inf_40 & filter_age_sup_16


@pytest.fixture
def filter_amount_empty(column_amount):
    return column_amount.is_empty()


@pytest.fixture
def filter_amount_sup_10(column_amount):
    return column_amount > 10


def test_get_df(df, filter_age_inf_40):
    res = filter_age_inf_40.get_df()
    assert list(res.index) == [0, 1, 2]
    assert list(res["id"]) == [1, 3, 4]
    assert list(res["name"]) == [None, "Andrew", "John"]
    assert list(res["age"]) == [18, 12, 32]
    # dropna because nan values can't be equal
    assert list(res["amount"].dropna()) == [18.9, 28.0]
    assert list(res["appointment_time"].dropna()) == [df["appointment_time"][0]]
    assert list(res["object"]) == [RandomClass(i) for i in [0, 2, 3]]
    assert list(res["agreement"]) == [np.nan, True, False]


def test_get_selected_ids(filter_age_inf_40):
    assert filter_age_inf_40.get_selected_ids() == set((1, 3, 4))


def test_copy(filter_age_inf_40):
    copy = filter_age_inf_40.copy()
    assert copy.df.equals(filter_age_inf_40.df)
    assert copy.id_colname == filter_age_inf_40.id_colname
    assert copy.data_colname == filter_age_inf_40.data_colname
    assert copy is not filter_age_inf_40


def test_invert(filter_age_inf_40):
    assert (~filter_age_inf_40).get_selected_ids() == set((2, 5))


def test_metafilter_get_selected_ids(simple_metafilter):
    assert simple_metafilter.get_selected_ids() == set((1, 4))


def test_metafilter_get_df(df, simple_metafilter):
    res = simple_metafilter.get_df()
    assert list(res.index) == [0, 1]
    assert list(res["id"]) == [1, 4]
    assert list(res["name"]) == [None, "John"]
    assert list(res["age"]) == [18, 32]
    # dropna because nan values can't be equal
    assert list(res["amount"].dropna()) == [18.9, 28.0]
    assert list(res["appointment_time"].dropna()) == [df["appointment_time"][0]]
    assert list(res["object"]) == [RandomClass(i) for i in [0, 3]]
    assert list(res["agreement"]) == [np.nan, False]


def test_metafilter_copy(simple_metafilter):
    copy = simple_metafilter.copy()
    assert copy.df.equals(simple_metafilter.df)
    assert copy.id_colname == simple_metafilter.id_colname
    assert copy.mode == simple_metafilter.mode
    assert copy is not simple_metafilter


def test_metafilter_invert(simple_metafilter):
    assert (~simple_metafilter).get_selected_ids() == set((2, 3, 5))


def test_complex_filter(
    simple_metafilter, filter_age_sup_16, filter_amount_empty, filter_amount_sup_10
):
    complex_filter = (
        simple_metafilter
        | filter_amount_sup_10
        | (filter_age_sup_16 & ~filter_amount_empty)
    )
    assert complex_filter.get_selected_ids() == set((1, 4, 5))


def test_readme():
    """
    Take the example of the readme to test it
    """
    df = pd.DataFrame(
        {
            "id": [1, 2, 3, 4, 5, 6],
            "col1": [1, 0, 8, 5, 3, 2],
            "col2": ["a", "b", "c", "d", "e", "f"],
        }
    )
    df2 = pd.DataFrame(
        {
            "id": [1, 2, 3, 4, 7, 8],
            "col1": [1, 0, 8, 5, 3, 2],
            "col3": ["a", "b", "c", "d", "e", "f"],
        }
    )

    col1 = Column(df, "id", "col1")
    col2 = Column(df, "id", "col2")
    col3 = Column(df2, "id", "col3")

    big_fucking_filter = (col1 <= 3) & col2.is_in(["a", "b", "f"]) | ~(
        col3.is_in(["a", "b", "e"])
    )

    assert big_fucking_filter.get_df().equals(
        pd.DataFrame(
            {
                "id": [1, 2, 3, 4, 6, 8],
                "col1": [1, 0, 8, 5, 2, np.nan],
                "col2": ["a", "b", "c", "d", "f", np.nan],
                "col3": ["a", "b", "c", "d", np.nan, "f"],
            }
        )
    )
