import pytest

from funnel.filters.combination_op import CombinationOp


@pytest.fixture
def set_a():
    return set([1, 2, 3, 4, 5, 10, 23, 42])


@pytest.fixture
def set_b():
    return set([1, 3, 6, 42, 128])


@pytest.fixture
def set_no_intersection():
    return set([0, 8, 11, 25, 100, 112])


@pytest.fixture
def set_empty():
    return set()


def test_UNION(set_a, set_b, set_no_intersection, set_empty):
    union_op = CombinationOp.UNION
    assert union_op.execute(set_a, set_a) == set_a
    assert union_op.execute(set_a, set_empty) == set_a
    assert union_op.execute(set_a, set_b) == set([1, 2, 3, 4, 5, 6, 10, 23, 42, 128])
    assert union_op.execute(set_a, set_no_intersection) == set(
        [1, 2, 3, 4, 5, 10, 23, 42, 0, 8, 11, 25, 100, 112]
    )
    assert union_op.execute(set_b, set_no_intersection) == set(
        [1, 3, 6, 42, 128, 0, 8, 11, 25, 100, 112]
    )


def test_INTERSECTION(set_a, set_b, set_no_intersection, set_empty):
    intersection_op = CombinationOp.INTERSECTION
    assert intersection_op.execute(set_a, set_a) == set_a
    assert intersection_op.execute(set_a, set_empty) == set_empty
    assert intersection_op.execute(set_a, set_b) == set([1, 3, 42])
    assert intersection_op.execute(set_a, set_no_intersection) == set()
    assert intersection_op.execute(set_b, set_no_intersection) == set()


def test_INVERT_UNION(set_a, set_b, set_no_intersection, set_empty):
    union_op = ~CombinationOp.UNION
    assert union_op.execute(set_a, set_a) == set_a
    assert union_op.execute(set_a, set_empty) == set_empty
    assert union_op.execute(set_a, set_b) == set([1, 3, 42])
    assert union_op.execute(set_a, set_no_intersection) == set()
    assert union_op.execute(set_b, set_no_intersection) == set()


def test_INVERT_INTERSECTION(set_a, set_b, set_no_intersection, set_empty):
    intersection_op = ~CombinationOp.INTERSECTION
    assert intersection_op.execute(set_a, set_a) == set_a
    assert intersection_op.execute(set_a, set_empty) == set_a
    assert intersection_op.execute(set_a, set_b) == set(
        [1, 2, 3, 4, 5, 6, 10, 23, 42, 128]
    )
    assert intersection_op.execute(set_a, set_no_intersection) == set(
        [1, 2, 3, 4, 5, 10, 23, 42, 0, 8, 11, 25, 100, 112]
    )
    assert intersection_op.execute(set_b, set_no_intersection) == set(
        [1, 3, 6, 42, 128, 0, 8, 11, 25, 100, 112]
    )
