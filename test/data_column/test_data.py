from datetime import datetime
from test.conftest import RandomClass

import numpy as np
import pandas as pd
import pytest

from funnel.exceptions import IncompatibleOperationException


def test_dtype(
    column_name,
    column_age,
    column_amount,
    column_appointment,
    column_object,
    column_agreement,
):
    assert pd.StringDtype.is_dtype(column_name.dtype)
    assert column_age.dtype == pd.Int64Dtype()
    assert repr(column_amount.dtype) == "Float64Dtype()"
    assert column_appointment.dtype == "datetime64[ns]"
    assert pd.core.dtypes.common.is_dtype_equal(column_object.dtype, np.object_)
    assert column_agreement.dtype == pd.BooleanDtype()


def test_bool(
    column_name,
    column_age,
    column_amount,
    column_appointment,
    column_object,
    column_agreement,
):
    assert column_name.__bool__()
    assert column_age.__bool__()
    assert column_amount.__bool__()
    assert column_appointment.__bool__()
    assert column_object.__bool__()
    assert column_agreement.__bool__()


def test_is_empty(
    column_name,
    column_age,
    column_amount,
    column_appointment,
    column_object,
    column_agreement,
):
    assert set([1]) == column_name.is_empty().get_selected_ids()
    assert set([2]) == column_age.is_empty().get_selected_ids()
    assert set([3]) == column_amount.is_empty().get_selected_ids()
    assert set([3, 4, 5]) == column_appointment.is_empty().get_selected_ids()
    assert set() == column_object.is_empty().get_selected_ids()
    assert set([1, 5]) == column_agreement.is_empty().get_selected_ids()


def test_identity(
    column_name,
    column_age,
    column_amount,
    column_appointment,
    column_object,
    column_agreement,
):
    assert set(range(1, 6)) == column_name.identity().get_selected_ids()
    assert set(range(1, 6)) == column_age.identity().get_selected_ids()
    assert set(range(1, 6)) == column_amount.identity().get_selected_ids()
    assert set(range(1, 6)) == column_appointment.identity().get_selected_ids()
    assert set(range(1, 6)) == column_object.identity().get_selected_ids()
    assert set(range(1, 6)) == column_agreement.identity().get_selected_ids()


def test_is_in(
    column_name,
    column_age,
    column_amount,
    column_appointment,
    column_object,
    column_agreement,
):
    assert (
        set([2, 4])
        == column_name.is_in(["Benjamin", "Nicolas", "John"]).get_selected_ids()
    )
    assert set([3]) == column_age.is_in(range(17)).get_selected_ids()
    assert set([2, 5]) == column_amount.is_in(np.array([1.2, 30.2])).get_selected_ids()
    assert (
        set([2])
        == column_appointment.is_in(
            (datetime.fromisoformat("2022-10-22 10:07:33.666"),)
        ).get_selected_ids()
    )
    assert (
        set([1, 4])
        == column_object.is_in([RandomClass(0), RandomClass(3)]).get_selected_ids()
    )
    assert set([2, 3]) == column_agreement.is_in([True]).get_selected_ids()


def test_is_decimal(
    column_name,
    column_age,
    column_amount,
    column_appointment,
    column_object,
    column_agreement,
):
    with pytest.raises(IncompatibleOperationException):
        column_name.is_decimal()
    with pytest.raises(IncompatibleOperationException):
        column_appointment.is_decimal()
    with pytest.raises(IncompatibleOperationException):
        column_object.is_decimal()
    with pytest.raises(IncompatibleOperationException):
        column_agreement.is_decimal()
    assert set([1, 3, 4, 5]) == column_age.is_decimal().get_selected_ids()
    assert set([4]) == column_amount.is_decimal().get_selected_ids()


def test_gt(
    column_name,
    column_age,
    column_amount,
    column_appointment,
    column_object,
    column_agreement,
):
    with pytest.raises(IncompatibleOperationException):
        column_name > 25
    assert set([4, 5]) == (column_age > 25).get_selected_ids()
    assert set([4, 5]) == (column_amount > 25).get_selected_ids()
    assert set([1, 2]) == (column_appointment > datetime(2010, 4, 1)).get_selected_ids()
    assert set([5]) == (column_object > 3).get_selected_ids()
    assert set([2, 3]) == (column_agreement > False).get_selected_ids()


def test_ge(
    column_name,
    column_age,
    column_amount,
    column_appointment,
    column_object,
    column_agreement,
):
    with pytest.raises(IncompatibleOperationException):
        column_name >= 25
    assert set([4, 5]) == (column_age >= 25).get_selected_ids()
    assert set([4, 5]) == (column_amount >= 25).get_selected_ids()
    assert (
        set([1, 2]) == (column_appointment >= datetime(2010, 4, 1)).get_selected_ids()
    )
    assert set([4, 5]) == (column_object >= 3).get_selected_ids()
    assert set([2, 3, 4]) == (column_agreement >= False).get_selected_ids()


def test_lt(
    column_name,
    column_age,
    column_amount,
    column_appointment,
    column_object,
    column_agreement,
):
    with pytest.raises(IncompatibleOperationException):
        column_name < 25
    assert set([1, 3]) == (column_age < 25).get_selected_ids()
    assert set([1, 2]) == (column_amount < 25).get_selected_ids()
    assert set() == (column_appointment < datetime(2010, 4, 1)).get_selected_ids()
    assert set([1, 2, 3]) == (column_object < 3).get_selected_ids()
    assert set([4]) == (column_agreement < True).get_selected_ids()


def test_le(
    column_name,
    column_age,
    column_amount,
    column_appointment,
    column_object,
    column_agreement,
):
    with pytest.raises(IncompatibleOperationException):
        column_name <= 25
    assert set([1, 3]) == (column_age <= 25).get_selected_ids()
    assert set([1, 2]) == (column_amount <= 25).get_selected_ids()
    assert set() == (column_appointment <= datetime(2010, 4, 1)).get_selected_ids()
    assert set([1, 2, 3, 4]) == (column_object <= 3).get_selected_ids()
    assert set([2, 3, 4]) == (column_agreement <= True).get_selected_ids()


def test_eq(
    column_name,
    column_age,
    column_amount,
    column_appointment,
    column_object,
    column_agreement,
):
    with pytest.raises(IncompatibleOperationException):
        column_name > 25
    assert set([4]) == (column_age == 32).get_selected_ids()
    assert set([4]) == (column_amount == 28).get_selected_ids()
    assert (
        set([2])
        == (
            column_appointment == datetime.fromisoformat("2022-10-22 10:07:33.666")
        ).get_selected_ids()
    )
    assert set([4]) == (column_object == 3).get_selected_ids()
    assert set([4]) == (column_agreement == False).get_selected_ids()


def test_ne(
    column_name,
    column_age,
    column_amount,
    column_appointment,
    column_object,
    column_agreement,
):
    with pytest.raises(IncompatibleOperationException):
        column_name > 25
    assert set([1, 2, 3, 5]) == (column_age != 32).get_selected_ids()
    assert set([1, 2, 3, 5]) == (column_amount != 28).get_selected_ids()
    assert (
        set([1, 3, 4, 5])
        == (
            column_appointment != datetime.fromisoformat("2022-10-22 10:07:33.666")
        ).get_selected_ids()
    )
    assert set([1, 2, 3, 5]) == (column_object != 3).get_selected_ids()
    assert set([1, 2, 3, 5]) == (column_agreement != False).get_selected_ids()
