from datetime import datetime

import numpy as np
import pandas as pd
import pytest

from funnel.data_column.data import Column


class RandomClass(object):
    def __init__(self, number) -> None:
        self.number = number

    def __eq__(self, o: object) -> bool:
        if isinstance(o, RandomClass):
            return o.number == self.number
        return o == self.number

    def __gt__(self, other) -> bool:
        return self.number > other

    def __ge__(self, other) -> bool:
        return self.number >= other

    def __lt__(self, other) -> bool:
        return self.number < other

    def __le__(self, other) -> bool:
        return self.number <= other

    def __ne__(self, other) -> bool:
        return not (self == other)


@pytest.fixture
def df():
    return pd.DataFrame(
        {
            "id": [1, 2, 3, 4, 5],  # int
            "name": [None, "Benjamin", "Andrew", "John", "Michael"],  # string
            "age": [18, np.nan, 12, 32, 67],  # int
            "amount": [18.9, 1.2, np.nan, 28.0, 30.2],  # float
            "appointment_time": [
                datetime.now(),
                datetime.fromisoformat("2022-10-22 10:07:33.666"),
                np.nan,
                None,
                np.nan,
            ],  # datetime
            "object": [RandomClass(i) for i in range(5)],  # object
            "agreement": [np.nan, True, True, False, np.nan],  # boolean
        }
    )


@pytest.fixture
def column_name(df):
    return Column(df, "id", "name")


@pytest.fixture
def column_age(df):
    return Column(df, "id", "age")


@pytest.fixture
def column_amount(df):
    return Column(df, "id", "amount")


@pytest.fixture
def column_appointment(df):
    return Column(df, "id", "appointment_time")


@pytest.fixture
def column_object(df):
    return Column(df, "id", "object")


@pytest.fixture
def column_agreement(df):
    return Column(df, "id", "agreement")
