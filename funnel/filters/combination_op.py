from enum import Enum


class CombinationOp(Enum):
    # TODO: optimization: use the mode on the filtering function then use the
    # resultant funtion on every item
    # -> less ram usage and probably better performance
    UNION = 1
    INTERSECTION = 2

    def execute(self, set_x, set_y):
        if self == CombinationOp.UNION:
            return set_x | set_y
        return set_x & set_y

    def invert(self):
        if self == CombinationOp.UNION:
            return CombinationOp.INTERSECTION
        return CombinationOp.UNION

    def __invert__(self):
        return self.invert()
